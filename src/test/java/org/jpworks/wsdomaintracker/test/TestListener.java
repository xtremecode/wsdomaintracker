package org.jpworks.wsdomaintracker.test;

import org.jpworks.wsdomaintracker.SystemRuntimeProperties;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestListener extends AbstractTestExecutionListener {
	
	@Override 
	public void beforeTestClass(TestContext testContext) throws Exception {
		log.debug("Inicialización del listener de pruebas");
	}
	
	@Override
	public void afterTestMethod(TestContext testContext)throws Exception {
		if (testContext.getTestException() != null){
			log.debug("Se detecto una excepción en el test");
			SystemRuntimeProperties.DETIENE_PRUEBAS = true;
		}
	}
	
	@Override 
	public void afterTestClass(TestContext testContext) throws Exception {
		log.debug("Finaliza el listener de pruebas");
	} 
}
