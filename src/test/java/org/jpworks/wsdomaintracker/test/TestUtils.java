package org.jpworks.wsdomaintracker.test;

import org.jpworks.wsdomaintracker.SystemRuntimeProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestUtils {

	public static void verificaContinuidadPruebas() throws TerminaTestException{
		if (SystemRuntimeProperties.DETIENE_PRUEBAS){
	    	throw new TerminaTestException("Se cancela test de integraci�n");
		}
	}
	
    public static HttpEntity<String> getRequestObject(Object objeto){
    	HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("charset", "ISO-8859-1");
        
        String respuestaJson = "";
    	try {
			respuestaJson = new ObjectMapper().writeValueAsString(objeto);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			respuestaJson = "{\"body\": \"Se gener� un error al convertir la respuesta que provenia de una excepci�n a formato json\"}";
		}
    	log.debug("***** orden: " + respuestaJson);
    	return new HttpEntity<String>(respuestaJson, headers);
    }
}
