package org.jpworks.wsdomaintracker.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.jpworks.wsdomaintracker.domain.UrlIn;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import lombok.extern.slf4j.Slf4j;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({ TestListener.class, DependencyInjectionTestExecutionListener.class})
public class IntegrationApiTest {
	
    @LocalServerPort
    private int port;

	@Value("${microservices.api.tracker.url}")
	private String serviceURL;
	
    @Autowired
    private TestRestTemplate restTemplate;
    
    String urlMicroservices = "";
    
    
    @Before public void setup() {
    	urlMicroservices = serviceURL.replace("$PORT", String.valueOf(port));
        log.debug("microservices.api.tracker.url: " + serviceURL);
    }
 
    @Test
    public void test1InvalidURL() throws Exception {
        log.debug("*********** Test: " + Thread.currentThread().getStackTrace()[1].getMethodName());
        TestUtils.verificaContinuidadPruebas();

        //Invalid URL
        UrlIn urlIn = new UrlIn();
        urlIn.setUrl("example.a.aail.google.com/mail/u/0/#inbox");
        
        ResponseEntity<String> response = restTemplate.postForEntity( urlMicroservices, TestUtils.getRequestObject(urlIn), String.class );
        log.debug(response.toString());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).contains("The URL is not valid");
    }
    
    @Test
    public void test2ValidDomain() throws Exception {
        log.debug("*********** Test: " + Thread.currentThread().getStackTrace()[1].getMethodName());
        TestUtils.verificaContinuidadPruebas();

        //Invalid URL
        UrlIn urlIn = new UrlIn();
        urlIn.setUrl("http://www.test.com/example");
        
        ResponseEntity<String> response = restTemplate.postForEntity( urlMicroservices, TestUtils.getRequestObject(urlIn), String.class );
        log.debug(response.toString());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
    
    @Test
    public void test3MostVisitedDomains() throws Exception {
        log.debug("*********** Test: " + Thread.currentThread().getStackTrace()[1].getMethodName());
        TestUtils.verificaContinuidadPruebas();

        //Invalid URL
        UrlIn urlIn = new UrlIn();
        urlIn.setUrl("http://www.test.com/example");
        
        ResponseEntity<String> response = restTemplate.postForEntity( urlMicroservices, TestUtils.getRequestObject(urlIn), String.class );
        log.debug(response.toString());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
}