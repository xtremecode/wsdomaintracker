package org.jpworks.wsdomaintracker.test;

@SuppressWarnings("serial")
public class TerminaTestException extends RuntimeException {

	public TerminaTestException(String message) {
        super(message);
    }

    public TerminaTestException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
