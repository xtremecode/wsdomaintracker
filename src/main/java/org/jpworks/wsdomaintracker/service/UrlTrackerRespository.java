package org.jpworks.wsdomaintracker.service;

import java.util.List;

import org.jpworks.wsdomaintracker.domain.jpa.UrlTracker;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UrlTrackerRespository extends CrudRepository<UrlTracker, Integer> { 
	@Query(value="select u.domain, count(u.domain) total from url_tracker u  group by domain order by total desc limit ?1", nativeQuery = true)
	List<Object[]> findByMostVisitedDomain(Integer totalRows);
}
