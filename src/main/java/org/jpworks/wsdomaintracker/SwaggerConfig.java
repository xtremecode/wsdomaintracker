package org.jpworks.wsdomaintracker;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          //.apis(RequestHandlerSelectors.any())
          .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
          .paths(PathSelectors.any()) 
          .build()
          .apiInfo(apiInfo())
          ;
    }
    private ApiInfo apiInfo() {
    	return new ApiInfoBuilder()
            .title("Domain Tracker API REST Specification")
            .description("This document is the specification of the REST API for Domain Tracker Management. It includes the model definition as well as all available operations. JpWorks testing Platform")
            .version("1.0")
            .contact(new Contact("Joaquin Ponte Diaz", "https://www.linkedin.com/in/joaquinponte/", "joaquin.ponte@gmail.com"))
            .license("Apache 2.0")
            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
            //.license("HTTP error codes description")
            //.licenseUrl("https://httpstatuses.com/")
            .termsOfServiceUrl("https://www.linkedin.com/in/joaquinponte/")
            .build();
    }
}