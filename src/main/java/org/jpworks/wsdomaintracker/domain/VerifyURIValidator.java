package org.jpworks.wsdomaintracker.domain;

import java.net.URI;
import java.net.URISyntaxException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerifyURIValidator implements ConstraintValidator<VerifyURI, String>{

	@Override
	public void initialize(VerifyURI valorAnotaciontipoCatalogo) {
	}

	@Override
	public boolean isValid(String valorUrl, ConstraintValidatorContext validatorContext) {
		boolean valid = false;
		
		try{
			new URI(valorUrl);
			valid = true;
		}
		catch(URISyntaxException e){
			//The URL is invalid
		}		
		
		log.debug("The URL is invalid: " + valorUrl);
		return valid;
	}
}
