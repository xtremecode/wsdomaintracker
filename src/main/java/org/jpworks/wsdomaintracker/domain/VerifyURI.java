package org.jpworks.wsdomaintracker.domain;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target( ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = VerifyURIValidator.class) 
public @interface VerifyURI {
	String message() default "It isn't a valid URL";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
