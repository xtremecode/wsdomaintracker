package org.jpworks.wsdomaintracker.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@JsonInclude(Include.NON_NULL)
@ApiModel(value="MostVisitedDomain", description="MostVisitedDomain")
@ToString(includeFieldNames=true)
@Data public class MostVisitedDomain{
	
	public final static String DOMAIN = "Domain of the website";
	public final static String TIMES = "Number of visits";
	
	@ApiModelProperty(value = DOMAIN, required= true, allowEmptyValue= false, example="xataka.com")	
    private String domain;
	
	@ApiModelProperty(value = TIMES, required= true, allowEmptyValue= false, example="33")	
    private String total;
}
