/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jpworks.wsdomaintracker.domain.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import org.jpworks.wsdomaintracker.domain.UrlIn;

/**
 *
 * @author jponte
 */
@Entity
@Table(name = "url_tracker")
@TypeDef(name = "jsonb", typeClass = JsonDataUserType.class, parameters = {
		@Parameter(name = JsonDataUserType.CLASS, value = "org.jpworks.wsdomaintracker.domain.UrlIn")})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UrlTracker.findAll", query = "SELECT u FROM UrlTracker u")
    , @NamedQuery(name = "UrlTracker.findByIdTrackerUrl", query = "SELECT u FROM UrlTracker u WHERE u.idTrackerUrl = :idTrackerUrl")
    , @NamedQuery(name = "UrlTracker.findByDomain", query = "SELECT u FROM UrlTracker u WHERE u.domain = :domain")
    , @NamedQuery(name = "UrlTracker.findByUrl", query = "SELECT u FROM UrlTracker u WHERE u.url = :url")
    , @NamedQuery(name = "UrlTracker.findByDate", query = "SELECT u FROM UrlTracker u WHERE u.date = :date")
    , @NamedQuery(name = "UrlTracker.findMostVisited", query = "SELECT u.domain, COUNT(u) FROM UrlTracker u GROUP BY u.domain")    
})
public class UrlTracker implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tracker_url")
    private Integer idTrackerUrl;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "domain")
    private String domain;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "url")
    private String url;
    
	@Type(type = "jsonb")
    @Column(name = "payload")
	private UrlIn urlIn;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public UrlTracker() {
    }

    public UrlTracker(Integer idTrackerUrl) {
        this.idTrackerUrl = idTrackerUrl;
    }

    public UrlTracker(Integer idTrackerUrl, String domain, String url, UrlIn urlIn, Date date) {
        this.idTrackerUrl = idTrackerUrl;
        this.domain = domain;
        this.url = url;
        this.urlIn = urlIn;
        this.date = date;
    }

    public Integer getIdTrackerUrl() {
        return idTrackerUrl;
    }

    public void setIdTrackerUrl(Integer idTrackerUrl) {
        this.idTrackerUrl = idTrackerUrl;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UrlIn getUrlIn() {
        return urlIn;
    }

    public void setUrlIn(UrlIn urlIn) {
        this.urlIn = urlIn;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTrackerUrl != null ? idTrackerUrl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UrlTracker)) {
            return false;
        }
        UrlTracker other = (UrlTracker) object;
        if ((this.idTrackerUrl == null && other.idTrackerUrl != null) || (this.idTrackerUrl != null && !this.idTrackerUrl.equals(other.idTrackerUrl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.jpworks.wsdomaintracker.domain.jpa.UrlTracker[ idTrackerUrl=" + idTrackerUrl + " ]";
    }
    
}
