package org.jpworks.wsdomaintracker.domain.jpa;

import java.sql.Types;

import org.hibernate.dialect.PostgreSQL94Dialect;

public class CustomPostgreSqlDialect extends PostgreSQL94Dialect {
	public CustomPostgreSqlDialect() {
		super();
		registerColumnType(Types.JAVA_OBJECT, JsonDataUserType.JSONB_TYPE);
	}
}
