package org.jpworks.wsdomaintracker.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@JsonInclude(Include.NON_NULL)
@ApiModel(value="UrlIn", description="UrlIn")
@ToString(includeFieldNames=true)
@Data public class UrlIn{
	
	public final static String NULO = " must not be null";
	public final static String VACIO = " must not be empty";

	//Fixed values that comes from Insurance Company
	public final static String URL = "URL of the website";

	//****************************************************
	//Fixed values
	//****************************************************
	@ApiModelProperty(value = URL, required= true, allowEmptyValue= false, example="https://www.xataka.com/historia-tecnologica/bill-gates-querria-volver-al-pasado-y-convertir-el-atajo-ctrl-alt-del-en-una-tecla-unica")	
	@NotNull(message = URL + NULO)
	@NotEmpty(message = URL + VACIO)
	@Size(min=1, max=200)
	@VerifyURI()
	private String url;
}
