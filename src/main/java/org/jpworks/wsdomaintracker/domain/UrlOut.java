package org.jpworks.wsdomaintracker.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@JsonInclude(Include.NON_NULL)
@ApiModel(value="UrlOut", description="UrlOut")
@ToString(includeFieldNames=true)
@Data public class UrlOut{
	
	public final static String ID = "ID of the record";
	public final static String URL = "URL of the website";
	public final static String DOMAIN = "Domain of the website";
	public final static String DATE = "Creation date of the record";

	@ApiModelProperty(value = ID, required= true, allowEmptyValue= false, example="23")	
    private Integer idTrackerUrl;

	@ApiModelProperty(value = URL, required= true, allowEmptyValue= false, example="https://www.xataka.com/historia-tecnologica")	
	private String url;
	
	@ApiModelProperty(value = DOMAIN, required= true, allowEmptyValue= false, example="xataka.com")	
    private String domain;
	
	@ApiModelProperty(value = DATE, required= true, allowEmptyValue= false, example="yyyy-MM-dd'T'HH:mm:ss:SSS")	
    private String date;
}
