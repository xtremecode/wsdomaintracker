package org.jpworks.wsdomaintracker.controller;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.jpworks.wsdomaintracker.domain.MostVisitedDomain;
import org.jpworks.wsdomaintracker.domain.UrlIn;
import org.jpworks.wsdomaintracker.domain.UrlOut;
import org.jpworks.wsdomaintracker.domain.jpa.UrlTracker;
import org.jpworks.wsdomaintracker.service.UrlTrackerRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(tags="Domain Tracker", value="Domain Tracker", description="Domain Tracker Management")
public class DomainTrackerController implements HealthIndicator {

	public final String GET_PATH = "/tracker/{idtrackerurl}";
	
	@Autowired
	private UrlTrackerRespository urlTrackerRepository;
	
    @Override
    public Health health() {
        int errorCode = check(); // perform some specific health check
        if (errorCode != 0) {
            return Health.down()
              .withDetail("Error Code", errorCode).build();
        }
        return Health.up().build();
    }
    
    public int check() {
        // Your logic to check health
        return 0;
    }
    
    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSS");

	@ApiOperation(
			value = "Save a domain from an URL",
			notes = "Save a domain from an URL",
			consumes="application/json"	)
	@ApiResponses(value = {
            @ApiResponse(code = 201, message = "The resource were created"),
			@ApiResponse(code = 400, message = "When the required parameter is missing or has a wrong type" ),
            @ApiResponse(code = 401, message = "The request has not been applied because it lacks valid authentication credentials for the target resource"),
            @ApiResponse(code = 403, message = "The server understood the request but refuses to authorize it"),
            @ApiResponse(code = 404, message = "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists"),
            @ApiResponse(code = 500, message = "The server encountered an unexpected condition that prevented it from fulfilling the request")
            })
	@RequestMapping(value="/tracker", method = RequestMethod.POST, 
						consumes = MediaType.APPLICATION_JSON_VALUE,
						produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin
    public ResponseEntity<?> tracker(
    				@ApiParam(name = "body", required=true, allowEmptyValue= false, value = "The new record for url")
    				@Valid @RequestBody UrlIn body,
    				UriComponentsBuilder ucBuilder) {    	
		HttpStatus codigoEjecucion = HttpStatus.BAD_REQUEST;
		HttpHeaders headers = null;
		
    	//Initialize UrlTracker
		UrlTracker urlTracker = new UrlTracker();
		urlTracker.setUrl(body.getUrl());
		String domain = "";
		try{
			domain = getHostName(body.getUrl());
			urlTracker.setDomain(domain);
			urlTracker.setDate(new Date());
			urlTracker.setUrlIn(body);
	    	urlTrackerRepository.save(urlTracker);
	        codigoEjecucion = HttpStatus.CREATED;
	        headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path(GET_PATH).buildAndExpand(urlTracker.getIdTrackerUrl()).toUri());
		}
		catch(Exception e){ 
			throw new TrackerServicesException("The URL is not valid");
		}
        return new ResponseEntity<String>(headers, codigoEjecucion);
    }
	
	private String getHostName(String url) throws URISyntaxException{
		URI uri = new URI(url);
		String hostname = uri.getHost();
		if (hostname != null) {
	        return hostname.startsWith("www.") ? hostname.substring(4) : hostname;
	    }
	    return hostname;
	}
	
	@ApiOperation(
			value = "Get the record from a saved url",
			notes = "Get the record from a saved url",
			consumes="application/json",
			response = org.jpworks.wsdomaintracker.domain.UrlOut.class
	)
	@ApiResponses(value = {
            @ApiResponse(code = 400, message = "When the required parameter is missing or has a wrong type" ),
            @ApiResponse(code = 401, message = "The request has not been applied because it lacks valid authentication credentials for the target resource"),
            @ApiResponse(code = 403, message = "The record requested doesn't exist, or the server understood the request but refuses to authorize it"),
            @ApiResponse(code = 404, message = "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists"),
            @ApiResponse(code = 500, message = "The server encountered an unexpected condition that prevented it from fulfilling the request"),
            })
	@RequestMapping(value="/tracker/{idtrackerurl}", method = RequestMethod.GET, 
						produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
    @CrossOrigin
    public ResponseEntity<?> tracker(
			@ApiParam(name = "idtrackerurl", required=true, allowEmptyValue= false, value = "Id of url tracked ")
			@PathVariable("idtrackerurl") String idTrakerUrl
			) {    	
		HttpStatus codigoEjecucion = HttpStatus.OK;
		UrlOut salida = null;

		UrlTracker urlTracker = null;
		urlTracker = urlTrackerRepository.findOne(Integer.valueOf(idTrakerUrl));
		    	
		//Verifica que la orden de servicio est� asignada a la aseguradora
		if (urlTracker != null){
			salida = new UrlOut();
			salida.setIdTrackerUrl(urlTracker.getIdTrackerUrl());
			salida.setDomain(urlTracker.getDomain());
			salida.setUrl(urlTracker.getUrl());
			salida.setDate(formatoFecha.format(urlTracker.getDate()));
		}
		else{
			codigoEjecucion = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<>(salida, codigoEjecucion);
	}
		
	@ApiOperation(
			value = "Get the 3 most visited domains",
			notes = "Get the 3 most visited domains",
			consumes="application/json",
			response = org.jpworks.wsdomaintracker.domain.MostVisitedDomain.class
	)
	@ApiResponses(value = {
            @ApiResponse(code = 400, message = "When the required parameter is missing or has a wrong type" ),
            @ApiResponse(code = 401, message = "The request has not been applied because it lacks valid authentication credentials for the target resource"),
            @ApiResponse(code = 403, message = "The record requested doesn't exist, or the server understood the request but refuses to authorize it"),
            @ApiResponse(code = 404, message = "The origin server did not find a current representation for the target resource or is not willing to disclose that one exists"),
            @ApiResponse(code = 500, message = "The server encountered an unexpected condition that prevented it from fulfilling the request"),
            })
	@RequestMapping(value="/tracker/treeMostVisited", 
				method = RequestMethod.GET, 
				produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseStatus(HttpStatus.OK)
    @CrossOrigin
    public ResponseEntity<?> treeMostVisited() {    	
		HttpStatus codigoEjecucion = HttpStatus.OK;
		
		LinkedList<MostVisitedDomain> out = null;
		List<Object[]> mostVisitedDomain = urlTrackerRepository.findByMostVisitedDomain(3);     	

		if (mostVisitedDomain != null){    		
    		out = new LinkedList<MostVisitedDomain>();
    		
    		for (Object[] domain : mostVisitedDomain) {
    			MostVisitedDomain element = new MostVisitedDomain();
    			element.setDomain((String)domain[0]);
    			BigInteger total = (BigInteger)domain[1];
    			element.setTotal(total.toString());
    			out.add(element);
    		}    	
    	}
    	else{
    		codigoEjecucion = HttpStatus.NOT_FOUND;
    	}
    	return new ResponseEntity<>(out, codigoEjecucion);
    }
}