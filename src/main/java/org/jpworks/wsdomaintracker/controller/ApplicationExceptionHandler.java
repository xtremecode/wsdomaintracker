package org.jpworks.wsdomaintracker.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ControllerAdvice  
public class ApplicationExceptionHandler {
	
	@ExceptionHandler(TrackerServicesException.class)
    protected ResponseEntity<String> handleResourceNotFound(TrackerServicesException ex){
		ApiError apiError = new ApiError(
			      HttpStatus.NOT_FOUND,
			      ex.getLocalizedMessage(),
			      "Errors found during execution");
      return respuesta(apiError);
    }
	
	static ResponseEntity<String> respuesta(ApiError apiError){
		String respuestaJson = "";
    	try {
			respuestaJson = new ObjectMapper().writeValueAsString(apiError);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			respuestaJson = "{\"body\": \"An error were generated during conversion format\"}";
		}
    	return ResponseEntity
                .status(apiError.getStatus())
                .body(respuestaJson);
	}
}