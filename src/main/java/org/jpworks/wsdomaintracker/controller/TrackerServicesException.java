package org.jpworks.wsdomaintracker.controller;

@SuppressWarnings("serial")
public class TrackerServicesException extends RuntimeException {

	public TrackerServicesException(String message) {
        super(message);
    }

    public TrackerServicesException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
