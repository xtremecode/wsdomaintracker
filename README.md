# JPWorks - Joaquin Ponte

This document is the specification of the REST API for Service Domian Tracker. It includes the model definition as well as all available operations. JpWorks testing Platform

Develop by [Joaquin Ponte](https://www.linkedin.com/in/joaquinponte/), September 2017.

 * Joaquin Ponte Diaz, joaquin.ponte@gmail.com

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## License

Code is under the [Apache Lincense](https://www.apache.org/licenses/LICENSE-2.0).

    
## Running the application locally

First build with:

	$mvn clean package

If you want to skip test validation:

    $mvn clean package -DskipTests


Then run it with:

	Linux/Windows:
	 $ java -jar target/wsdomaintracker-1.0.jar
    
	A complete integrated line with maven:
	 cls && mvn package && java -jar target\wsdomaintracker-1.0.jar
	
Local

	To point in the local environment:

	http://localhost:8080/wsdomaintracker

	To point to heroku:
	
	https://wsdomaintracker.herokuapp.com/wsdomaintracker
	
Swagger

	User interface= http://localhost:8080/swagger-ui.html
	Swagger definition= http://localhost:8080/v2/api-docs
	
