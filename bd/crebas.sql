/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     15/07/2017 01:45:54 p. m.                    */
/*==============================================================*/


drop table ASEGURADORA;

drop table ORDENSERVICIO;

drop table ORDENSERVICIO_BITACORA;

drop table ORDENSERVICIO_ESTADO;

drop table SERVICE_LOG;

drop table TIPO_SERVICIO;

/*==============================================================*/
/* Table: ASEGURADORA                                           */
/*==============================================================*/
create table ASEGURADORA (
   CVE_ASEGURADORA      VARCHAR(12)          not null,
   DESC_ASEGURADORA     VARCHAR(100)         not null,
   ID_ASEGURADORA_IKE   INT4                 null,
   constraint PK_ASEGURADORA primary key (CVE_ASEGURADORA)
);

/*==============================================================*/
/* Table: ORDENSERVICIO                                         */
/*==============================================================*/
create table ORDENSERVICIO (
   ID_ORDENSERVICIO     SERIAL               not null,
   CVE_ESTADO_SERVICIO  VARCHAR(10)          not null,
   CVE_TIPOSERVICIO     VARCHAR(10)          not null,
   CVE_ASEGURADORA      VARCHAR(10)          not null,
   EXPEDIENTE_IKE       INT8                 not null,
   ASEGURADORA_OPERADORNOMBRE VARCHAR(100)         not null,
   ASEGURADORA_FOLIOSISTEMA VARCHAR(50)          not null,
   USUARIO_NOMBRE       VARCHAR(100)         not null,
   USUARIO_APELLIDO_PATERNO VARCHAR(100)         not null,
   USUARIO_APELLIDO_MATERNO VARCHAR(100)         null,
   TELEFONO_PRINCIPAL_LADA VARCHAR(6)           not null,
   TELEFONO_PRINCIPAL   VARCHAR(20)          not null,
   TELEFONO_ALTERNO     VARCHAR(20)          null,
   POLIZA_NUMERO        VARCHAR(50)          not null,
   POLIZA_NOMBRE        VARCHAR(100)         not null,
   ES_COBRABLE_A_USUARIO BOOL                 not null,
   ES_SERVICIO_ESPECIAL BOOL                 not null,
   AUTO_TIPO            VARCHAR(30)          null,
   AUTO_MARCA           VARCHAR(30)          null,
   AUTO_ANIO            VARCHAR(10)          null,
   AUTO_COLOR           VARCHAR(30)          null,
   AUTO_PLACAS          VARCHAR(10)          null,
   AUTO_SERIEMOTOR      VARCHAR(20)          null,
   ES_AUTO_BLINDADO     BOOL                 null,
   POLIZA_DIR_CALLENUMERO VARCHAR(100)         null,
   POLIZA_DIR_REFERENCIA VARCHAR(200)         null,
   POLIZA_DIR_COLONIA   VARCHAR(100)         null,
   POLIZA_DIR_MUNICIPIO VARCHAR(100)         null,
   POLIZA_DIR_ENTIDAD   VARCHAR(100)         null,
   POLIZA_DIR_CODPOS    VARCHAR(5)           null,
   FECHA_ALTA           TIMESTAMP            not null,
   FECHA_MODIFICACION   TIMESTAMP            not null,
   constraint PK_ORDENSERVICIO primary key (ID_ORDENSERVICIO)
);

/*==============================================================*/
/* Table: ORDENSERVICIO_BITACORA                                */
/*==============================================================*/
create table ORDENSERVICIO_BITACORA (
   ID_ORDENSERVICIO_BITACORA SERIAL               not null,
   CVE_ESTADO_SERVICIO  VARCHAR(10)          null,
   ID_ORDENSERVICIO     INT4                 null,
   COMENTARIOS          VARCHAR(200)         null,
   FECHA_ALTA           TIMESTAMP            not null,
   constraint PK_ORDENSERVICIO_BITACORA primary key (ID_ORDENSERVICIO_BITACORA)
);

/*==============================================================*/
/* Table: ORDENSERVICIO_ESTADO                                  */
/*==============================================================*/
create table ORDENSERVICIO_ESTADO (
   CVE_ESTADO_SERVICIO  VARCHAR(10)          not null,
   DESC_ESTADO_SERVICIO VARCHAR(100)         not null,
   ID_ESTADO_IKE_PK1    INT2                 null,
   ID_ESTADO_IKE_PK2    INT2                 null,
   constraint PK_ORDENSERVICIO_ESTADO primary key (CVE_ESTADO_SERVICIO)
);

/*==============================================================*/
/* Table: SERVICE_LOG                                           */
/*==============================================================*/
create table SERVICE_LOG (
   ID_TRANSACTION       SERIAL not null,
   SERVICE              VARCHAR(70)          not null,
   OPERATION            VARCHAR(100)         not null,
   EXECUTION_TIME_MS    INT4                 not null,
   EXECUTION_CODE       VARCHAR(100)         null,
   EXECUTION_DESCRIPTION VARCHAR(100)         null,
   TRACE                TEXT                 not null,
   OPERATION_TICKET     CHAR(36)             not null,
   PARENT_OPERATION_TICKET CHAR(36)             null,
   HTTP_STATUS_CODE     INT2                 not null,
   SERVICE_ERROR        BOOL                 not null,
   DATE_TRANSACTION     TIMESTAMP            not null,
   constraint PK_SERVICE_LOG primary key (ID_TRANSACTION)
);

/*==============================================================*/
/* Table: TIPO_SERVICIO                                         */
/*==============================================================*/
create table TIPO_SERVICIO (
   CVE_TIPOSERVICIO     VARCHAR(20)          not null,
   DESC_TIPOSERVICIO    VARCHAR(100)         not null,
   ID_TIPOSERVICIO_IKE  INT4                 null,
   constraint PK_TIPO_SERVICIO primary key (CVE_TIPOSERVICIO)
);

alter table ORDENSERVICIO
   add constraint FK_ORDENSER_REFERENCE_ORDENSER foreign key (CVE_ESTADO_SERVICIO)
      references ORDENSERVICIO_ESTADO (CVE_ESTADO_SERVICIO)
      on delete restrict on update restrict;

alter table ORDENSERVICIO
   add constraint FK_ORDENSER_REFERENCE_TIPO_SER foreign key (CVE_TIPOSERVICIO)
      references TIPO_SERVICIO (CVE_TIPOSERVICIO)
      on delete restrict on update restrict;

alter table ORDENSERVICIO
   add constraint FK_ORDENSER_REFERENCE_ASEGURAD foreign key (CVE_ASEGURADORA)
      references ASEGURADORA (CVE_ASEGURADORA)
      on delete restrict on update restrict;

alter table ORDENSERVICIO_BITACORA
   add constraint ORDENSERVICIO_BITACORA_FK01 foreign key (CVE_ESTADO_SERVICIO)
      references ORDENSERVICIO_ESTADO (CVE_ESTADO_SERVICIO)
      on delete restrict on update restrict;

alter table ORDENSERVICIO_BITACORA
   add constraint ORDENSERVICIO_BITACORA_FK02 foreign key (ID_ORDENSERVICIO)
      references ORDENSERVICIO (ID_ORDENSERVICIO)
      on delete restrict on update restrict;

